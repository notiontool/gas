## 概要

- GASで動作するNotion用の便利ツールです

|関数名|用途|
|---|---|
|updateWebClipperReadDate|WebClipperのReadチェックボックスにチェックあり かつ <br> ReadDateが空のページに日付を入力する<br> タイマートリガーで30分ごとに動かす等|
|makeHabitTrackerDatePage|習慣トラッカー用のデータベースに日付のページを作成する<br>日次トリガーでAM1:00～2:00に動かす等|

## 設定方法


[Wiki](https://gitlab.com/notiontool/gas/-/wikis/home)を参照してください
