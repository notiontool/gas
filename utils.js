/**
 * ページの内容を更新する
 */
function patchPageData(props, pageId, payload) {
  let url = "https://api.notion.com/v1/pages/" + pageId
  let options = {
    "method" : "PATCH",
    "headers": {
      "Content-type": "application/json",
      "Authorization": "Bearer " + props.NOTION_TOKEN,
      "Notion-Version": props.NOTION_VERSION,
    },
    "payload": JSON.stringify(payload)
  };

  try {
    let res = UrlFetchApp.fetch(url, options);
    Logger.log(res);
    return JSON.parse(res);
  } catch (e) {
    Logger.log(e);
    return undefined;
  }

}

/**
 * DBから指定条件に合うものをfetchする
 */
function fetchDatabaseData(props, databaseId, payload) {
  let url = "https://api.notion.com/v1/databases/" + databaseId + "/query"
  let options = {
    "method" : "POST",
    "headers": {
      "Content-type": "application/json",
      "Authorization": "Bearer " + props.NOTION_TOKEN,
      "Notion-Version": props.NOTION_VERSION,
    },
    "payload" : JSON.stringify(payload),
    "muteHttpExceptions" : true
  }

  try {
    let res = UrlFetchApp.fetch(url, options);
    Logger.log(res);
    return JSON.parse(res);
  } catch (e) {
    Logger.log(e);
    return undefined;
  }
}

/**
 * 新規でページを作成する
 */
function postPage(props, payload) {
  let url = "https://api.notion.com/v1/pages";
  let options = {
    "method": "POST",
    "headers": {
      "Content-type": "application/json",
      "Authorization": "Bearer " + props.NOTION_TOKEN,
      "Notion-Version": props.NOTION_VERSION,
    },
    "payload": JSON.stringify(payload),
    "muteHttpExceptions": true
  };

  try {
    let res = UrlFetchApp.fetch(url, options);
    Logger.log(res)
    return JSON.parse(res);
  } catch (e) {
    Logger.log(e);
    return undefined;
  }
}

