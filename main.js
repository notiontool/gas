/**
 * WebClipperのDBのReadチェック済み かつ ReadDate が空のページに
 * 日付を入れる(タイマー実行にする)
 */
function updateWebClipperReadDate() {
  let props = PropertiesService.getScriptProperties().getProperties();

  let payloadFetch = {
    "filter": {
      "and": [
        {
          "property": "Read",
          "checkbox": {
            "equals": true
          }
        },
        {
          "property": "ReadDate",
          "date": {
            "is_empty": true
          }
        }
      ]
    },
  }
  let list = fetchDatabaseData(props, props.DATABASE_ID_WEBCLIP, payloadFetch);

  if (list.results.length === 0) {
    Logger.log("更新対象なし")
    return;
  }


  let date_str = Utilities.formatDate(new Date(), "JST", "yyyy-MM-dd");
  let payloadPatch = {
    "properties": {
      "ReadDate": {
        "date": {
          "start": date_str
        }
      }
    }
  };

  list.results.forEach(t => {
    let pageId = t["id"]
    patchPageData(props, pageId, payloadPatch)
    Utilities.sleep(1000);
  });

}

/**
 * 習慣トラッカー用の日付ページを作成する
 * (日次実行にする) 
 */
function makeHabitTrackerDatePage() {
  let props = PropertiesService.getScriptProperties().getProperties();
  let d = new Date();
  let tomorrow = new Date(d.setDate(d.getDate() + 1));


  let payload = {
    "parent": {
      "database_id": props.DATABASE_ID_HABIT
    },
    "properties": {
      "Name": {
        "title": [
          {
            "text": {
              "content": Utilities.formatDate(tomorrow, "JST", "yyyy/MM/dd")
            }
          }
        ]
      },
      "Date": {
        "date": {
          "start": Utilities.formatDate(tomorrow, "JST", "yyyy-MM-dd")
        }
      }
    }
  }
  Logger.log(props.DATABASE_ID_HABIT)
  postPage(props, payload);
}
