/**
 * 手動実行で設定する
 * (新しいGASエディタだとGUIでの設定ができないため・・・古いエディタに切り替えての設定でも可)
 */
function setScriptProps() {
  let scriptProperties = PropertiesService.getScriptProperties();

  scriptProperties.setProperties({
    'NOTION_VERSION': '2021-08-16',
    'NOTION_TOKEN': '',
    'DATABASE_ID_WEBCLIP': '',
    'DATABASE_ID_HABIT': ''
  });
}
